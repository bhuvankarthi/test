import TabComponents from "./components/TabComponents";
import "./App.css";
import Impact from "./components/Pages/Impact/Impact";
import Footer from "./components/Footer";
import AboutUs from "./components/Pages/About Us/AboutUs";
import Home from "./components/Pages/Home/Home";
import { Route, Routes } from "react-router";
import { useEffect } from "react";
import { useNavigate } from "react-router";

function App() {
  const navigate = useNavigate();
  useEffect(() => {
    navigate("/");
  }, []);
  return (
    <div className="App">
      <TabComponents />
      <Routes>
        <Route path="/" exact element={<Home />} />
        <Route path="/impact" exact element={<Impact />} />
        <Route path="/aboutUs" exact element={<AboutUs />} />
      </Routes>

      <Footer />
    </div>
  );
}

export default App;
