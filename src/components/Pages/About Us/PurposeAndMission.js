import React from "react";
import "../Impact/Impact.css";
import nature from "../../../images/Aboutus/nature.png";
import auto from "../../../images/Aboutus/auto.png";
import bunk from "../../../images/Aboutus/bunk.png";
import coinhand from "../../../images/Aboutus/coinhand.png";
import loan from "../../../images/Aboutus/loan.png";
import { AiFillApple } from "react-icons/ai";
import { FaGooglePlay } from "react-icons/fa";
import { AiOutlineRight } from "react-icons/ai";
const PurposeAndMission = () => {
  return (
    <div
      className="container-fluid d-flex  justify-content-center"
      style={{ backgroundColor: "#F5F5F5", marginTop: "130px" }}
    >
      <div className="row  ">
        <div className="container d-flex  justify-content-center">
          <div className="row d-felx flex-column justify-content-center">
            <div className=" column d-flex flex-column align-items-center  p-5 mt-5">
              <p style={{ fontSize: "30px", fontWeight: "500" }}>Purpose</p>
              <p style={{ color: "gray", fontSize: "18px" }}>
                Reduce pollution while generating more income for people <br />{" "}
                in the Global South
              </p>
            </div>
          </div>
        </div>

        <div className="column d-md-flex p-5">
          <img
            src={nature}
            className="p-3 d-md-none rounded"
            style={{ width: "100%" }}
          />
          <div style={{ width: "40%" }}>
            <img
              src={nature}
              className="p-3 d-none d-md-block d-lg-none rounded"
              style={{ width: "100%" }}
            />
            <img
              src={nature}
              className="p-3 d-none d-lg-block rounded"
              style={{ width: "400px" }}
            />
          </div>

          <div className="d-md-none ">
            <h3>Mission</h3>
            <p className="p-3 text-secondary">
              Be the leading financier for income generating electric mobility
              in emerging markets. Three Wheels United is a Global tech-enhanced
              financier for light electric vehicles in emerging markets,
              starting with the auto rickshaws in India. TWU is combining
              tailored financing solutions with innovative loan management and
              asset management technology to offer a competitive loan, removing
              barriers for uptake, while de-risking their lending. As an
              organization we are committed to reinvesting majority of our
              profits in order to scale our business and it's impact on poverty
              and pollution reduction.
            </p>
          </div>
          <div
            className="d-none d-md-block text-left ml-3 mt-5 align-items-center"
            style={{ width: "60%" }}
          >
            <h3>Mission</h3>
            <p className="  text-secondary">
              Be the leading financier for income generating electric mobility
              in emerging markets. Three Wheels United is a Global tech-enhanced
              financier for light electric vehicles in emerging markets, <br />
              <br />
              starting with the auto rickshaws in India. TWU is combining
              tailored financing solutions with innovative loan management and
              asset management technology to offer a competitive loan, removing
              barriers for uptake, while de-risking their lending.
              <br />
              <br /> As an organization we are committed to reinvesting majority
              of our profits in order to scale our business and it's impact on
              poverty and pollution reduction.
            </p>
          </div>
        </div>

        <div className="container">
          <div className="row ">
            <div className="container d-flex justify-content-center mt-5 mt-md-1 we-have-done ">
              <div className="row">
                <div className="column">
                  <h3>What We've Done?</h3>
                  <div className="d-flex  flex-column flex-md-row ">
                    <div className="d-flex flex-row justify-content-center align-items-center bg-success rounded  m-1 m-md-5 p-2 mt-3 ">
                      <div>
                        <img
                          src={auto}
                          className="mr-2"
                          style={{ width: "60%" }}
                        />
                      </div>
                      <div className="text-light">
                        <h3 style={{ fontWeight: "500" }}>5000+</h3>
                        <p>Vehicles Financed</p>
                      </div>
                    </div>
                    <div
                      className="d-flex flex-row justify-content-center align-items-center rounded bg-light m-1 m-md-5 p-2 mt-3  "
                      style={{ boxShadow: "0 0 10px #D8F1E9" }}
                    >
                      <div>
                        <img
                          src={bunk}
                          className="mr-2"
                          style={{ width: "60%" }}
                        />
                      </div>
                      <div>
                        <h3>172,000</h3>
                        <p>Co-2 reduced</p>
                      </div>
                    </div>
                  </div>
                  <div className="d-flex  flex-column flex-md-row">
                    <div
                      className="d-flex flex-row justify-content-center align-items-center rounded  bg-light m-1 m-md-5 p-2 mt-3 "
                      style={{ boxShadow: "0 0 10px #D8F1E9" }}
                    >
                      <div>
                        <img
                          src={coinhand}
                          className="mr-2"
                          style={{ width: "60%" }}
                        />
                      </div>
                      <div>
                        <h3>$71 M+</h3>
                        <p>Extra Income Generated</p>
                      </div>
                    </div>
                    <div
                      className="d-flex flex-row justify-content-center align-items-center rounded  bg-light m-1 m-md-5 p-2 mt-3 "
                      style={{ boxShadow: "0 0 10px #D8F1E9" }}
                    >
                      <div>
                        <img
                          src={loan}
                          className="mr-2"
                          style={{ width: "60%" }}
                        />
                      </div>
                      <div>
                        <h3>{`<`}1%</h3>
                        <p>Defaulted Loans</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* For downloading app */}

        <div className="d-flex flex-column align-items-center justify-content-center col-12">
          <h4 className="mb-5 mt-5">Download our app to get started today!</h4>
          <p className="mb-3 text-secondary">
            End-to-end payments and financial management in a single solution.
          </p>
          <div className="d-flex flex-column flex-md-row">
            <button
              className=" d-flex align-items-center text-light  pr-5 pl-5 pt-2 pb-2 mb-3 mt-3 mr-md-5"
              style={{
                borderRadius: "20px",
                border: "none",
                backgroundColor: "#fd6300",
              }}
            >
              <AiFillApple className="mr-2" />
              Store
            </button>
            <button
              className="d-flex align-items-center text-light pr-4 pl-4 pt-2 pb-2 mb-3 mt-3 "
              style={{
                borderRadius: "20px",
                border: "none",
                backgroundColor: "#50b748",
              }}
            >
              <FaGooglePlay className="mr-2" />
              Google Play
            </button>
          </div>
        </div>
        <div className=" col-12 mt-5 d-flex flex-column  align-items-center">
          <h2>
            Subscribe Newsletter & <br />
            Company Update
          </h2>
          <div className="d-flex col-md-4   align-items-center mb-3 ">
            <input
              type="email"
              class="form-control mt-5 mt-3"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              placeholder="Enter Your email"
              style={{ borderRadius: "20px", width: "100%" }}
            />
            <button
              className="mt-5 p-1 pr-2 pl-2"
              style={{
                borderRadius: "20px",
                backgroundColor: "#fd6300",
                border: "none",
                marginLeft: "-30px",
                width: "80px",
              }}
            >
              <AiOutlineRight style={{ color: "white" }} />
            </button>
          </div>
          <hr style={{ height: "2px", width: "80%" }} />
        </div>
      </div>
    </div>
  );
};

export default PurposeAndMission;
