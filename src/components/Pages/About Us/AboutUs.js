import React from "react";
import "../Impact/Impact.css";
//image imports
import group from "../../../images/Aboutus/group.png";
import PurposeAndMission from "./PurposeAndMission";
const AboutUs = () => {
  return (
    <div>
      <div className="container " style={{ marginTop: "75px" }}>
        <div className="row about-us">
          <div className="column">
            <div className="d-flex flex-column align-items-center    p-3">
              <h2
                className="mb-5 text-left  col-md-10"
                style={{
                  textAlign: "left",
                }}
              >
                Dedicated Teams.
                <br />
                For You Dedicated Dreams
              </h2>

              <div className="d-md-none">
                <img
                  src={group}
                  className="mb-3 "
                  style={{ width: "90%", height: "90%" }}
                />
              </div>
              <div
                className="rounded d-md-none"
                style={{
                  backgroundColor: "#72FCA5",
                  width: "90%",
                  height: "90%",
                  opacity: "1",
                  color: "black",
                  padding: "5px",
                }}
              >
                <h3 className="text-dark">Why do we Do This?</h3>
                <p className="text-dark p-3" style={{ textAlign: "left" }}>
                  Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                  diam nonumy eirmod tempor invidunt ut labore et dolore magna
                  aliquyam erat, sed diam voluptua. At vero eos et accusam et
                  justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                </p>
                <button
                  className="pl-3 pr-3 pt-2 pb-2 mb-2"
                  style={{
                    borderRadius: "20px",
                    backgroundColor: "#fd6300",
                    border: "none",
                    color: "white",
                  }}
                >
                  Read for Blog
                </button>
              </div>
              <div className="d-none d-md-block ">
                <img
                  src={group}
                  className="mb-3 p-0"
                  style={{ width: "80%", height: "80%" }}
                />
                <div
                  className="rounded"
                  style={{
                    backgroundColor: "#72FCA5",
                    marginTop: "-60px",
                    marginLeft: "240px",
                    position: "absolute",
                    width: "400px",
                    opacity: ".9",
                    color: "black",
                    padding: "5px",
                  }}
                >
                  <h3 className="text-dark">Why do we Do This?</h3>
                  <p className="text-dark p-3" style={{ textAlign: "left" }}>
                    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                    diam nonumy eirmod tempor invidunt ut labore et dolore magna
                    aliquyam erat, sed diam voluptua. At vero eos et accusam et
                    justo duo dolores et ea rebum. Stet clita kasd gubergren, no
                  </p>
                  <button
                    className="pl-3 pr-3 pt-2 pb-2 mb-2"
                    style={{
                      borderRadius: "20px",
                      backgroundColor: "#fd6300",
                      border: "none",
                      color: "white",
                    }}
                  >
                    Read for Blog
                  </button>
                </div>
              </div>
            </div>

            {/* <div className="">
            
          </div> */}
          </div>
        </div>
      </div>

      <PurposeAndMission />
    </div>
  );
};

export default AboutUs;
