import React from "react";
import { TiTick } from "react-icons/ti";
const LoanOption = ({ items }) => {
  console.log("hii");
  const {
    vehicle,
    Installment,
    InstallmentColor,
    para,
    price,
    Asset,
    Tenure,
    Funding,
    color,
    bgColor,
    pix,
    tickColor,
    buttonColor,
  } = items;
  return (
    <div
      className="p-3 mt-5"
      style={{
        backgroundColor: bgColor,
        color: color,
        borderRadius: "20px",
        boxShadow: `0px 0px ${pix}px #D8F1E9`,
      }}
    >
      <p style={{ color: tickColor }}>{vehicle}</p>

      <p className="d-flex align-items-center">
        <span>
          <h1 className="mr-3" style={{ color: InstallmentColor }}>
            ${Installment}
          </h1>
        </span>
        Per month
      </p>

      <p className="p-1 text-left" style={{ fontSize: "14px" }}>
        {para}
      </p>
      <hr />
      <div>
        <div className="d-flex">
          <div className="d-flex text-left" style={{ width: "50%" }}>
            <TiTick
              className="mt-1 mr-2"
              style={{
                color: bgColor,
                backgroundColor: tickColor,
                borderRadius: "50%",
              }}
            />
            <p className="mr-3">Ticket Size</p>
          </div>
          <p className="text-left" style={{ width: "50%", fontWeight: "500" }}>
            {`INR `}
            {price}
          </p>
        </div>
        <div className="d-flex">
          <div className="d-flex " style={{ width: "50%" }}>
            <TiTick
              className="mt-1 mr-2"
              style={{
                color: bgColor,
                backgroundColor: tickColor,
                borderRadius: "50%",
              }}
            />
            <p className="mr-3">Asset</p>
          </div>
          <p className="text-left" style={{ width: "50%", fontWeight: "500" }}>
            {Asset}
          </p>
        </div>
        <div className="d-flex">
          <div className="d-flex " style={{ width: "50%" }}>
            <TiTick
              className="mt-1 mr-2"
              style={{
                color: bgColor,
                backgroundColor: tickColor,
                borderRadius: "50%",
              }}
            />
            <p className="mr-3">Tenure</p>
          </div>

          <p className=" text-left" style={{ width: "50%", fontWeight: "500" }}>
            {Tenure}
          </p>
        </div>
        <div className="d-flex justify-content-between">
          <div className="d-flex " style={{ width: "50%" }}>
            <TiTick
              className="mt-1 mr-2"
              style={{
                color: bgColor,
                backgroundColor: tickColor,
                borderRadius: "50%",
              }}
            />
            <p className="mr-3">Funding</p>
          </div>

          <p className=" text-left" style={{ width: "50%", fontWeight: "500" }}>
            {Funding}
          </p>
        </div>
        <button
          className="p-1 pr-5 pl-5 pl-2 mb-3"
          style={{
            backgroundColor: buttonColor,
            color: bgColor,
            border: "none",
            borderRadius: "20px",
          }}
        >
          Learn more
        </button>
      </div>
    </div>
  );
};

export default LoanOption;
