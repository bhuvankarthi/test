import React from "react";
import { AiOutlineRight } from "react-icons/ai";
import rain from "../../../images/46.png";
import sound from "../../../images/Group.png";
import lady from "../../../images/People and earth/lady.png";
import earth from "../../../images/People and earth/earth.png";
import review1 from "../../../images/People and earth/reviewPerson1.png";
import comma from "../../../images/People and earth/comma.png";
import fiveStars from "../../../images/People and earth/fiveStars.png";
const AdvantagesAndFeedback = () => {
  return (
    <div>
      <div>
        <div className=" pt-5 pb-5 mt-5">
          <div className="container">
            <div className="row ">
              <div className="column d-md-flex justify-content-center align-items-center">
                <div
                  style={{
                    borderRadius: "10px",
                    // boxShadow: "0px 0px 20px #D8F1E9",
                  }}
                >
                  <div
                    className=" d-flex justify-content-end "
                    style={{ position: "relative", top: "20px" }}
                  >
                    <img src={rain} style={{ width: "20%" }} />
                  </div>
                  <div
                    style={{ backgroundColor: "aliceblue", height: "250px" }}
                  >
                    <img
                      src={lady}
                      style={{
                        position: "relative",
                        bottom: "50px",

                        height: "300px",

                        borderRadius: "15px",
                      }}
                    />
                  </div>
                  <div
                    className="d-flex justify-content-start "
                    style={{
                      position: "relative",
                      bottom: "20px",
                      right: "15px",
                    }}
                  >
                    <img src={sound} style={{ width: "15%" }} />
                  </div>
                </div>
                <div className="col-md-6 text-left pl-5 mt-md-0 mt-5">
                  <h3>Taking Fintech Loan Management to Next Level</h3>
                  <p style={{ color: "gray" }}>
                    Clubbing loan collections, asset management, income
                    generation, and behavioral data to create a suite of
                    solutions that work best for our clients
                  </p>
                  <button
                    className="pl-3 pr-2"
                    style={{
                      borderRadius: "20px",
                      border: "none",
                      backgroundColor: "#fd6300",
                      color: "white",
                    }}
                  >
                    see how <AiOutlineRight className="m-2" />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        class="delivery-and-payment-section pt-5 pb-5 d-flex"
        id="delivery-payment"
      >
        <div class="container">
          <div class="row">
            <div className="col-2"></div>
            <div class="col-12 col-md-5 order-1 order-md-2">
              <div class="text-center">
                <div className=" d-flex justify-content-start">
                  <img
                    className="ml-md-5"
                    src={rain}
                    style={{
                      width: "10%",
                      marginBottom: "-40px",
                      transform: "rotate(90deg)",
                    }}
                  />
                </div>
                <div>
                  <img
                    src={earth}
                    alt=""
                    style={{
                      width: "60%",
                      height: "50%",
                      backgroundColor: "aliceblue",
                      borderRadius: "10px",
                    }}
                  />
                </div>
                <div className=" d-flex justify-content-end ">
                  <img
                    className="mr-md-5"
                    src={sound}
                    style={{
                      width: "10%",
                      marginTop: "-40px",
                      transform: "rotate(240deg)",
                    }}
                  />
                </div>
              </div>
            </div>
            <div class=" col-12 col-md-5 order-2 order-md-1 text-left pl-5 mt-md-3 mt-5 ">
              <p style={{ fontSize: "30px", fontWeight: "500" }}>
                Discover a better way to take action for the Planet
              </p>
              <p style={{ color: "gray" }}>
                Measure your carbon footprint, reduce, contribute to carbon
                offsetting projects. Three Wheels United is the trusted partner
                for your path to net-zero.
              </p>
              <button
                className="pl-3 pr-2 mt-md-5"
                style={{
                  borderRadius: "20px",
                  border: "none",
                  backgroundColor: "#50b748",
                  color: "white",
                }}
              >
                see how
                <AiOutlineRight className="m-2" />
              </button>
            </div>
          </div>
        </div>
      </div>

      <p
        style={{
          color: "#50b748",

          fontFamily: "SegoeUI-Bold, Segoe UI",
          fontWeight: "700",
        }}
      >
        TESTIMONIALS
      </p>

      <p style={{ fontSize: "30px", fontWeight: "500" }}>
        Check what our clients are saying
      </p>
      <div>
        <div class="healthy-food-section pt-5 pt-md-2 pb-5">
          <div class="container">
            <div class="row">
              <div class="col-12 col-md-5">
                <div class="">
                  <div className="d-flex justify-content-start  pl-5">
                    <img
                      className="ml-md-5 mt-md-5"
                      src={sound}
                      style={{ width: "10%", transform: "rotate(70deg)" }}
                    />
                  </div>
                  <div>
                    <img
                      src={review1}
                      style={{
                        width: "40%",
                        height: "250px",
                        backgroundColor: "aliceblue",
                        borderRadius: "15px",
                      }}
                    />
                  </div>
                  <div className="d-flex justify-content-end pr-5">
                    <img
                      className="mr-md-5 mb-md-5"
                      src={sound}
                      style={{ width: "10%", transform: "rotate(240deg)" }}
                    />
                  </div>
                </div>
              </div>
              <div class=" col-12 col-md-5 text-left pl-5 mt-md-0 mt-5 d-flex flex-column justify-content-center">
                <img className="mb-3" src={comma} style={{ width: "5%" }} />
                <img
                  className="mb-3"
                  src={fiveStars}
                  style={{ width: "50%", height: "7%" }}
                />

                <p className="mb-3" style={{ fontWeight: "700", width: "80%" }}>
                  Is be upon sang fond must shew. Really boy law county she
                  unable her sister. Feet you off its like like six. Among sex
                  are leave law built now.
                </p>
                <p className=" mb-0" style={{ fontWeight: "700" }}>
                  AR Shakir
                </p>
                <p style={{ fontSize: "14px" }} className=" text-secondary ">
                  Driver at TWU
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdvantagesAndFeedback;
