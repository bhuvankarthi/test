import React from "react";
import birdsLike from "../../../images/62.png";
import user from "../../../images/SignIn Icons/user.png";
import settings from "../../../images/SignIn Icons/settings.png";
import thunder from "../../../images/SignIn Icons/thunder.png";
import downCurve from "../../../images/SignIn Icons/downCurve.png";
import upCurve from "../../../images/SignIn Icons/upCurve.png";

//menu components
import LoanOption from "./LoanOption";
import AdvantagesAndFeedback from "./AdvantagesAndFeedback";
import MediaAndSignUp from "./MediaAndSignUp";

//icons for apple store and gpay
import { AiFillApple } from "react-icons/ai";
import { FaGooglePlay } from "react-icons/fa";
const SignUpSteps = () => {
  return (
    <div
      className="container-fluid d-flex  justify-content-center pt-5"
      style={{ backgroundColor: "#F8F8F8" }}
    >
      <div className="row">
        <div className="column ">
          <div>
            <div className="col-12 d-flex flex-column align-items-center">
              <img src={birdsLike} alt="birds" className="pb-3" />
              <p
                className="col-md-6"
                style={{ fontSize: "35px", fontWeight: "400" }}
              >
                Financing Last Mile Drivers
              </p>
              <p
                className="col-md-9"
                style={{
                  fontSize: "20px",
                  color: "gray",
                  fontFamily: "SegoeUI, Segoe UI",
                }}
              >
                Tailored Finance for Electric Light Vehicles
              </p>
            </div>

            <div class="container ">
              <div class="row  d-md-flex flex-md-row d-flex flex-column align-items-center justify-content-center mt-5">
                <div class="column">
                  <div class=" mb-3 d-flex flex-column  align-items-center">
                    <p
                      className="p-1 pr-2 pl-2"
                      style={{
                        backgroundColor: "white",
                        borderRadius: "50%",
                        boxShadow: "0px 0px 20px #D8F1E9",
                        fontWeight: "500",
                      }}
                    >
                      1
                    </p>
                    <img
                      className="p-2"
                      src={user}
                      style={{
                        backgroundColor: "white",
                        width: "30%",
                        borderRadius: "20px",
                      }}
                    />
                    <h4 class="mt-3 pb-3">Register</h4>
                    <p class="text-secondary">Join us to enjoy the Services</p>
                  </div>
                </div>
                <div className=" column d-none d-lg-block ">
                  <img src={upCurve} style={{ width: "50%", height: "40%" }} />
                </div>

                <div class="column ">
                  <div class=" mb-3 d-flex flex-column align-items-center">
                    <p style={{ fontWeight: "500" }}>2</p>
                    <img src={settings} style={{ width: "30%" }} />
                    <h4 class=" mt-3 pb-3">Complete Set-Up</h4>
                    <p class=" text-secondary ">Complete the Process</p>
                  </div>
                </div>
                <div className=" column d-none d-lg-block">
                  <img
                    src={downCurve}
                    style={{ width: "50%", height: "40%" }}
                  />
                </div>
                <div class="column">
                  <div class="  mb-3">
                    <p style={{ fontWeight: "500" }}>3</p>
                    <img src={thunder} style={{ width: "30%" }} />
                    <h4 class=" mt-3 pb-3">Utilize App</h4>
                    <p class=" text-secondary">
                      Utilize all the Properties of TWU
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container d-flex justify-content-center">
            <div className="row mt-5">
              <div className="column ">
                <p
                  className="mb-5 "
                  style={{ fontWeight: "500", fontSize: "25px" }}
                >
                  Download our app to get Started Today
                </p>
                <p className="mb-5 text-secondary ">
                  End to end payments and Financial Management in a single
                  Solution
                </p>
                <div className="container mb-5 d-flex flex-column flex-md-row align-items-center  justify-content-center ">
                  <button
                    className=" d-flex align-items-center  text-light  pr-5 pl-5 pt-2 pb-2 pb-md-2 pt-md-2 mb-3 mb-md-0 mr-md-3"
                    style={{
                      borderRadius: "20px",
                      border: "none",
                      backgroundColor: "#fd6300",
                    }}
                  >
                    <AiFillApple className="mr-2" />
                    Store
                  </button>
                  <button
                    className="d-flex align-items-center text-light pr-4 pl-4 pt-2 pb-2"
                    style={{
                      borderRadius: "20px",
                      border: "none",
                      backgroundColor: "#50b748",
                    }}
                  >
                    <FaGooglePlay className="mr-2" />
                    Google Play
                  </button>
                </div>
              </div>
            </div>
          </div>

          {/* <div className="container d-flex justify-content-center">
                  <div className="row">
                    <div className="column  d-md-flex">
                      <LoanOption
                        items={{
                          vehicle: "3 Wheeler",
                          Installment: "$0",
                          price: "210,000",
                          Asset: "Electric Three Wheeler",
                          Tenure: "Up To 45 Months",
                          Funding: "100%",
                          color: "gray",
                          textColor: "black",
                          bgColor: "white",
                          tickColor: "#50b748",
                        }}
                      />

                      <LoanOption
                        items={{
                          vehicle: "3 Wheeler",
                          Installment: "$0",
                          price: "210,000",
                          Asset: "Electric Three Wheeler",
                          Tenure: "Up To 45 Months",
                          Funding: "100%",
                          color: "white",
                          textColor: "white",
                          bgColor: "#50b748",
                          tickColor: "white",
                        }}
                      />

                      <LoanOption
                        items={{
                          vehicle: "3 Wheeler",
                          Installment: "$0",
                          price: "210,000",
                          Asset: "Electric Three Wheeler",
                          Tenure: "Up To 45 Months",
                          Funding: "100%",
                          color: "gray",
                          textColor: "black",
                          bgColor: "white",
                          tickColor: "#50b748",
                        }}
                      />
                    </div>
                  </div>
                </div> */}
          {/* </div>
            </div>
          </div> */}
          <div class=" pt-5 pb-5">
            <div class="container" style={{ backgroundColor: "white" }}>
              <div class="row">
                <div class="col-12">
                  <p
                    className="mt-5 "
                    style={{ fontWeight: "500", fontSize: "32px" }}
                  >
                    Select our loan option that suits you!
                  </p>
                  <p
                    className="d-none d-md-block mt-3 "
                    style={{ color: "gray" }}
                  >
                    With lots of unique and useful features, You can easily
                    <br /> manage your wallet easily without any Problem
                  </p>
                  <p className="d-md-none mt-3" style={{ color: "gray" }}>
                    With lots of unique and useful features, You can easily
                    manage your wallet easily without any Problem
                  </p>
                </div>
                <div class="col-12 col-md-4">
                  <div class=" p-3 mb-3">
                    <LoanOption
                      items={{
                        vehicle: "2 Wheeler Loan",
                        Installment: "0",
                        InstallmentColor: "black",
                        para: "Joy horrible moreover man feelings own shy. Request norland neither mistake for yet. Between the for morning assured.",
                        ticket: "210,000",
                        Asset: "Electric 2-Wheeler",
                        Tenure: "Up To 45 Months",
                        Funding: "100%",
                        color: "gray",
                        pix: "40",
                        installmentColor: "black",
                        bgColor: "white",
                        tickColor: "#50b748",
                        buttonColor: "#50b748",
                      }}
                    />
                  </div>
                </div>
                <div class="col-12 col-md-4">
                  <div class="p-3 mb-3">
                    <LoanOption
                      items={{
                        vehicle: "3 Wheeler Loan",
                        Installment: "49",
                        InstallmentColor: "white",
                        para: "On even feet time have an no at. Relation so in confined smallest children unpacked delicate. Why sir end believe.",
                        price: "210,000",
                        Asset: "Electric 3-Wheeler",
                        Tenure: "Up To 45 Months",
                        Funding: "100%",
                        color: "white",
                        pix: "150",
                        buttonColor: "white",
                        bgColor: "#50b748",
                        tickColor: "white",
                      }}
                    />
                  </div>
                </div>
                <div class="col-12 col-md-4">
                  <div class="wcu-card p-3 mb-3">
                    <LoanOption
                      items={{
                        vehicle: "3 Wheeler Loan",
                        Installment: "0",
                        InstallmentColor: "black",
                        para: "Joy horrible moreover man feelings own shy. Request norland neither mistake for yet. Between the for morning assured.",
                        price: "210,000",
                        Asset: "Electric 3 Wheeler",
                        Tenure: "Up To 45 Months",
                        Funding: "100%",
                        color: "gray",
                        pix: "40",
                        bgColor: "white",
                        tickColor: "#50b748",
                        buttonColor: "#50b748",
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <AdvantagesAndFeedback />

          <MediaAndSignUp />
        </div>
      </div>
    </div>
  );
};

export default SignUpSteps;
