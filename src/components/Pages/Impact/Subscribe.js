import React from "react";
import { AiOutlineRight } from "react-icons/ai";
const Subscribe = () => {
  return (
    <div
      className="container-fluid p-5 d-flex justify-content-center"
      style={{ backgroundColor: "#F5F5F5" }}
    >
      <div className="d-md-none">
        <div
          className="d-none container d-flex flex-column justify-content-center align-items-center mt-5"
          style={{ width: "100%" }}
        >
          <h1>Subscribe News Letter & Company Updates</h1>
          <input
            type="email"
            class="form-control mt-md-5 mt-3"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
            placeholder="Enter Your email"
            style={{ borderRadius: "20px", width: "100%" }}
          />
          <button
            className="mt-2"
            style={{
              borderRadius: "20px",
              backgroundColor: "#fd6300",
              border: "none",
              color: "white",
              width: "25%",
            }}
          >
            Go <AiOutlineRight style={{ color: "white" }} />
          </button>
        </div>
      </div>
      <div className="d-none d-md-block">
        <div
          className="d-none container d-flex flex-column justify-content-center mt-5"
          style={{ width: "70%" }}
        >
          <h1>Subscribe News Letter & Company Updates</h1>
          <div className="d-flex   align-items-center ">
            <input
              type="email"
              class="form-control mt-5 mt-3"
              id="exampleInputEmail1"
              aria-describedby="emailHelp"
              placeholder="Enter Your email"
              style={{ borderRadius: "20px", width: "100%" }}
            />
            <button
              className="mt-5 ml-2 p-1 pr-2 pl-2"
              style={{
                borderRadius: "20px",
                backgroundColor: "#fd6300",
                border: "none",
              }}
            >
              <AiOutlineRight style={{ color: "white" }} />
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Subscribe;
