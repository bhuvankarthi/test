import React from "react";
import { AiOutlineRight } from "react-icons/ai";
import scholar from "../../../images/scholar.png";
import autoBgBlur from "../../../images/autoBgBlur.png";
import town from "../../../images/town.png";
import cheers from "../../../images/cheers.png";
import child from "../../../images/child.png";
import taxi from "../../../images/taxi.png";
import man from "../../../images/Image 3.png";

//component
import Solutions from "./Solutions";
import Subscribe from "./Subscribe";
import "./Impact.css";
const Impact = () => {
  return (
    <div>
      <div
        className="container  impact "
        style={{
          backgroundRepeat: "none",
          marginTop: "75px",
        }}
      >
        {/* d-flex flex-column justify-content-center flex-md-row */}
        <div className="row ">
          <div className="column d-md-flex align-items-md-center rounded">
            <div className=" d-flex flex-column align-items-center">
              <p style={{ fontSize: "30px", fontWeight: "500", width: "60%" }}>
                Creating Global Impact
              </p>
              <p className=" p-3">
                Building efficient income for drivers, their families & reducing
                carbon emission in the environment
              </p>
              <button
                className="text-secondary  bg-light p-2"
                style={{
                  border: "none",
                  borderRadius: "20px",
                  fontWeight: "300",
                }}
              >
                Find out How
                <AiOutlineRight
                  className="ml-3 "
                  style={{
                    cursor: "pointer",
                    backgroundColor: "#fd6300",
                    color: "white",
                    borderRadius: "50%",
                    padding: "5px",
                  }}
                />
              </button>
            </div>
            <div className=" d-none d-md-block ">
              <img src={man} style={{ width: "50%" }} />
            </div>
            <div className="  d-md-flex flex-md-column ">
              <img
                src={scholar}
                className="col-sm-6 mb-3 mt-5 rounded"
                style={{ width: "60%" }}
              />
              <img
                src={autoBgBlur}
                className="col-sm-6 pb-5 rounded"
                style={{ width: "60%" }}
              />
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid bg-light mt-5 ">
        <h2 className="mb-5 " style={{ color: "charcoal" }}>
          Our Services
        </h2>
        <div className="row  ">
          {/* <h2 className="mb-5 ">Solutions</h2> */}
          <div className="column  d-md-flex mb-md-5">
            <div className="col-md-5">
              <img
                src={cheers}
                style={{ width: "45%", height: "80%" }}
                className="mb-5 rounded"
              />
            </div>
            <div className=" col-md-6 d-md-flex flex-md-column    text-md-left">
              <p
                className="d-none d-md-block mt-5"
                style={{ fontSize: "30px", fontWeight: "500", width: "60%" }}
              >
                Creating incomes for drivers
              </p>
              <p
                className="d-md-none"
                style={{ fontSize: "30px", fontWeight: "500", width: "100%" }}
              >
                Creating incomes for drivers
              </p>
              <p className="text-secondary mt-3">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren, no
              </p>
              {/* //#fd6300 */}
              <button
                className="p-2 mt-3"
                style={{
                  backgroundColor: " #50b748",
                  color: "white",
                  border: "none",
                  borderRadius: "20px",
                  width: "150px",
                }}
              >
                Learn More <AiOutlineRight />
              </button>
              <hr style={{ height: "1px", backgroundColor: "gray" }} />
            </div>
          </div>

          {/* <div className="container col-sm-12 justify-content-center">
              
              <div>
                
                <hr style={{ height: "2px", backgroundColor: "gray" }} />
              </div>
            </div> */}
          <div className="column d-md-flex  mb-md-5">
            <div className="col-md-5">
              <img
                src={child}
                style={{ width: "45%", height: "80%" }}
                className="mb-5 rounded"
              />
            </div>

            <div className=" col-md-6 d-md-flex flex-md-column    text-md-left">
              <p
                className="d-none d-md-block mt-5"
                style={{ fontSize: "30px", fontWeight: "500", width: "60%" }}
              >
                Providing basic needs for their families
              </p>
              <p
                className="d-md-none"
                style={{ fontSize: "30px", fontWeight: "500", width: "100%" }}
              >
                Providing basic needs for their families
              </p>

              <p className="text-secondary mt-3">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren, no
              </p>
              {/* //#fd6300 */}
              <button
                className="p-2 mt-3"
                style={{
                  backgroundColor: " #fd6300",
                  color: "white",
                  border: "none",
                  borderRadius: "20px",
                  width: "150px",
                }}
              >
                Learn More <AiOutlineRight />
              </button>
              <hr style={{ height: "1px", backgroundColor: "gray" }} />
            </div>
          </div>
          <div className="column d-md-flex  mb-md-5">
            <div className="col-md-5">
              <img
                src={taxi}
                style={{ width: "45%", height: "80%" }}
                className="mb-5 rounded"
              />
            </div>

            <div className=" col-md-6 d-md-flex flex-md-column    text-md-left">
              <p
                className="d-none d-md-block mt-5"
                style={{ fontSize: "30px", fontWeight: "500", width: "60%" }}
              >
                Providing transport solutions
              </p>
              <p
                className="d-md-none"
                style={{ fontSize: "30px", fontWeight: "500", width: "100%" }}
              >
                Providing transport solutions
              </p>

              <p className="text-secondary mt-3">
                Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed
                diam nonumy eirmod tempor invidunt ut labore et dolore magna
                aliquyam erat, sed diam voluptua. At vero eos et accusam et
                justo duo dolores et ea rebum. Stet clita kasd gubergren, no
              </p>
              {/* //#fd6300 */}
              <button
                className="p-2 mt-3"
                style={{
                  backgroundColor: " #50b748",
                  color: "white",
                  border: "none",
                  borderRadius: "20px",
                  width: "150px",
                }}
              >
                Learn More <AiOutlineRight />
              </button>
              <hr style={{ height: "1px", backgroundColor: "gray" }} />
            </div>
          </div>

          {/* <AiOutlineRight /> */}
        </div>
      </div>
      <Solutions />
      <Subscribe />
    </div>
  );
};

export default Impact;
