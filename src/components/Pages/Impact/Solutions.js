import React from "react";

//images import

import hands from "../../../images/media/hands.png";
import handsKey from "../../../images/media/handsKey.png";
import handsRupee from "../../../images/media/handsRupee.png";
import computer from "../../../images/media/computer.png";

const Solutions = () => {
  return (
    <div
      className="container-fluid solutions"
      style={{ backgroundColor: "#F5F5F5" }}
    >
      <h2 className="mb-5 pt-5">Our Solutions</h2>
      <div className="row d-flex flex-sm-column justify-content-center flex-row">
        <div className="d-md-flex  ">
          <div className="column col-md-4 align-self-center">
            <p style={{ fontSize: "30px", fontWeight: "500" }}>
              How we solve for it!
            </p>
          </div>
          <div className="column d-flex flex-column p-5 col-md-4 align-self-start">
            <div
              className="column bg-light p-3 mb-5 d-flex flex-column align-items-center"
              style={{ boxShadow: "0px 0px 20px #D8F1E9" }}
            >
              <img src={hands} style={{ width: "20%" }} className="mb-5" />
              <h5 style={{ textAlign: "left" }}>Flexible Renting solution</h5>
              <p className="small" style={{ textAlign: "left", color: "gray" }}>
                With our 7 years + experience delivering exceptional strategies
                that grow healthcare centers, you can rest assured that we’ll
                always have your back. Unlike other agencies that do all stuff,
                we specialize in healthcare and so we’re able to produce
                campaigns that meet your patients’ needs and boost growth.
              </p>
            </div>
            <div
              className="column bg-light p-3 d-flex flex-column align-items-center"
              style={{ boxShadow: "0px 0px 20px #D8F1E9" }}
            >
              <img src={handsKey} style={{ width: "20%" }} className="mb-5" />
              <h5 style={{ textAlign: "left" }}>
                Affordable Loans at competitive rates
              </h5>
              <p style={{ textAlign: "left", color: "gray" }} className="small">
                Already 200+ healthcare owners and practitioners trust us to
                manage their marketing. The question is, are you next? Not only
                would we assist you to increase appointment, but we’ll also work
                with you to streamline your processes and reduce waiting room
                time.
              </p>
            </div>
          </div>
          <div className="d-flex flex-column p-5 col-md-4 align-self-end">
            <div
              className="column bg-light p-3 d-flex flex-column mb-5 align-items-center"
              style={{ boxShadow: "0px 0px 20px #D8F1E9" }}
            >
              <img src={handsRupee} style={{ width: "20%" }} className="mb-5" />
              <h5 style={{ textAlign: "left" }}>Rent to own</h5>
              <p style={{ textAlign: "left", color: "gray" }} className="small">
                We know that proving return on investment is very important to
                you. So as experts, everything we do is poised at providing
                value for money by delivery consistent results. We guarantee an
                increased inflow of new patients, retention of existing ones and
                streamlined processes that save you time.
              </p>
            </div>
            <div
              className="column bg-light p-3 d-flex flex-column align-items-center"
              style={{ boxShadow: "0px 0px 20px #D8F1E9" }}
            >
              <img src={computer} style={{ width: "20%" }} className="mb-5" />
              <h5 style={{ textAlign: "left" }}>Increasing income by 75%</h5>
              <p style={{ textAlign: "left", color: "gray" }} className="small">
                Each solution we provide is highly customized and built for your
                unique challenges. We work closely with you and your team to
                develop personalized solutions that meet your budget and drive
                constant growth faster.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Solutions;
