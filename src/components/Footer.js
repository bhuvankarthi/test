import React from "react";
import logo from "../images/threeWheelsLogo.png";

//image imports
import fb from "../images/social/fb.png";
import youTube from "../images/social/youTube.png";
import insta from "../images/social/insta.png";

const Footer = () => {
  return (
    <div
      className="container-fluid pt-3"
      style={{ backgroundColor: "#F5F5F5" }}
    >
      {/* <hr style={{ margin: "0" }} /> */}
      <div className="row ">
        <div className="column col-12 d-md-flex justify-content-between align-items-center ">
          <div className="mt-3 mb-2">
            <img src={logo} />
          </div>
          <div
            className=" d-md-flex flex-md-row  align-items-center justify-content-center mt-3 mt-md-0"
            style={{ textAlign: "center" }}
          >
            <p className="mr-3 " style={{ fontWeight: "500" }}>
              About
            </p>
            <p className="mr-3 " style={{ fontWeight: "500" }}>
              Products
            </p>
            <p className="mr-3 " style={{ fontWeight: "500" }}>
              Impact
            </p>
            <p className="mr-3 " style={{ fontWeight: "500" }}>
              Contact Us
            </p>
          </div>
          <div className="mt-3">
            <a
              href="https://www.facebook.com/Threewheelsunited/"
              target="_blank"
            >
              <img src={fb} className="m-3" />
            </a>

            <img src={youTube} className="m-3" />
            <img src={insta} className="m-3" />
          </div>
        </div>
      </div>
      <div className="row d-flex  justify-content-center pb-2 pt-2">
        <div className="column d-md-flex">
          <p>Copyright © 2022.</p>
          <a href="">threewheelsunited.com</a>
        </div>
      </div>
    </div>
  );
};

export default Footer;
