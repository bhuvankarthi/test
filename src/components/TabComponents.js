import React, { useState } from "react";
import logo from "../images/threeWheelsLogo.png";
import menu from "../images/Menu.png";
import Home from "./Pages/Home/Home";
import Impact from "./Pages/Impact/Impact";
import { Link } from "react-router-dom";
import "./TabComponent.css";
const TabComponents = () => {
  const [active, setActive] = useState("Home");
  const activeHandler = (e) => {
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
    setActive(e.target.textContent);
  };
  return (
    <div>
      <nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top pt-4 pb-2 logo">
        <div class="container d-flex flex-row justify-content-between">
          <div class="d-flex flex-row align-items-center mr-5">
            <img src={logo} />
          </div>

          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div
            class="collapse navbar-collapse    font-weight-bold"
            id="navbarNavAltMarkup"
          >
            <ul class="navbar-nav mr-auto" onClick={(e) => activeHandler(e)}>
              <Link to="/">
                <li
                  class={`nav-item p-2 text-${
                    active === "Home" ? "dark" : "secondary"
                  }`}
                >
                  Home
                </li>
              </Link>
              <Link to="/product">
                <li
                  class={`nav-item p-2 text-${
                    active === "Product" ? "dark" : "secondary"
                  }`}
                >
                  Product
                </li>
              </Link>
              <Link to="/impact">
                <li
                  class={`nav-item p-2 text-${
                    active === "Impact" ? "dark" : "secondary"
                  }`}
                >
                  Impact
                </li>
              </Link>
              <Link to="/aboutUs">
                <li
                  class={`nav-item p-2 text-${
                    active === "About Us" ? "dark" : "secondary"
                  }`}
                >
                  About Us
                </li>
              </Link>
            </ul>

            {/* <div class="d-flex flex-row justify-content-center ">
                <a class="nav-link active custom-nav-link ">Home </a>
                <a class="nav-link custom-nav-link">Product</a>
                <a class="nav-link custom-nav-link">Impact</a>
                <a class="nav-link custom-nav-link">About Us</a>
              </div> */}

            <div>
              <button className="carbon-print-button pr-4 pl-4 pt-2 pb-2 ">
                Check Carbon Footprint
              </button>
            </div>
          </div>
        </div>
      </nav>
      {/* <Home /> */}
    </div>
  );
};

export default TabComponents;
